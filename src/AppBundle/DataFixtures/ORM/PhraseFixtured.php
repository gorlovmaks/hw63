<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Phrase;
use AppBundle\Entity\PhraseTranslation;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class PhraseFixtured implements FixtureInterface, ORMFixtureInterface
{
    private $encoder;



    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     *
     */
    public function load(ObjectManager $manager)
    {

        $User = new User();

        $password = $this->encoder->encodePassword($User,'123');
        $User
            ->setUsername('Fredrih10')
            ->setEmail('User1@mail.ru')
            ->setPassword($password)
            ->setEnabled(true);

        $manager->persist($User);

        $User = new User();
        $User
            ->setUsername('Fredrih20')
            ->setEmail('User2@mail.ru')
            ->setPassword($password)
            ->setEnabled(true);

        $manager->persist($User);

        $User = new User();
        $User
            ->setUsername('Fredrih30')
            ->setEmail('User3@mail.ru')
            ->setPassword($password)
            ->setEnabled(true);
        $manager->persist($User);

        $phrase = new Phrase();

        $phrase->translate('ru',false)->setPhrase('Супер Фраза');
        $phrase->translate('fr',false)->setPhrase('Super Phrase');
        $phrase->translate('en',false)->setPhrase('Super Phrase');
        $phrase->translate('de',false)->setPhrase('Super Angebot');
        $phrase->translate('it',false)->setPhrase('Frase super');
        $phrase->translate('ip',false)->setPhrase('Súper frase');

        $phrase->mergeNewTranslations();
        $manager->persist($phrase);

        $phrase = new Phrase();

        $phrase->translate('ru',false)->setPhrase('Супер Фраза');
        $phrase->translate('fr',false)->setPhrase('Super Phrase');
        $phrase->translate('en',false)->setPhrase('Super Phrase');
        $phrase->translate('de',false)->setPhrase('Super Angebot');
        $phrase->translate('it',false)->setPhrase('Frase super');
        $phrase->translate('ip',false)->setPhrase('Súper frase');

        $phrase->mergeNewTranslations();
        $manager->persist($phrase);

        $phrase = new Phrase();

        $phrase->translate('ru',false)->setPhrase('Супер Фраза');
        $phrase->translate('fr',false)->setPhrase('Super Phrase');
        $phrase->translate('en',false)->setPhrase('Super Phrase');
        $phrase->translate('de',false)->setPhrase('Super Angebot');
        $phrase->translate('it',false)->setPhrase('Frase super');
        $phrase->translate('ip',false)->setPhrase('Súper frase');

        $phrase->mergeNewTranslations();
        $manager->persist($phrase);
        $manager->flush();
    }

}
