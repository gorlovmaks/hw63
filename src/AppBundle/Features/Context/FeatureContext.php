<?php

namespace AppBundle\Features\Context;

use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpKernel\KernelInterface;


/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements KernelAwareContext
{
    /** @var  KernelInterface */
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }


    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }


    /**
     * @When /^я перехожу на главную страницу$/
     */
    public function яПерехожуНаГлавнуюСтраницу()
    {
        $url = $this->getContainer()
            ->get('router')
            ->generate('app_phrase_index');
        $this->visit($url);
    }

    /**
     * @When /^я перехожу на страницу регистрации$/
     */
    public function яПерехожуНаСтраницуРегистрации()
    {
        $url = $this->getContainer()
            ->get('router')
            ->generate('fos_user_registration_register');
        $this->visit($url);
    }

    /**
     * @When /^я заполняю поля данными$/
     * @param TableNode $fields
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function fillFields(TableNode $fields)
    {
        foreach ($fields->getRowsHash() as $field => $value) {
            $this->fillField($field, $value);
        }

    }

    /**
     * @Then /^Я нажимаю на "([^"]*)"$/
     * @param $element
     */
    public function iClickOn($element)
    {
        $page = $this->getSession()->getPage();
        $findName = $page->find("css", $element);
        if (!$findName) {
            throw new Exception($element . " could not be found");
        } else {
            $findName->click();
        }
    }


    /**
     * @When /^я перехожу на страницу авторизации$/
     */
    public function яПерехожуНаСтраницуАвторизации()
    {
        $url = $this->getContainer()
            ->get('router')
            ->generate('fos_user_security_login');
        $this->visit($url);
    }

    /**
     * @When /^заполняю поля данными$/
     * @param TableNode $fields
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function IgoToTheAuthorizationPage(TableNode $fields)
    {

        foreach ($fields->getRowsHash() as $field => $value) {
            $this->fillField($field, $value);
        }
    }


    /**
     *
     * @When /^Я нажимаю на ссылку добавления фраз"((?:[^"]|\\")*)"$/
     * @param $link
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function clickLink($link)
    {
        $link = $this->fixStepArgument($link);
        $this->getSession()->getPage()->clickLink($link);
    }

    /**
     * Fills in form field with specified id|name|label|value
     * Example: When I fill in "username" with: "bwayne"
     * Example: And I fill in "bwayne" for "username"
     *
     * @When /^Добавляю новую фразу в "((?:[^"]|\\")*)" со значением "((?:[^"]|\\")*)"$/
     * @param $field
     * @param $value
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function fillField($field, $value)
    {
        $field = $this->fixStepArgument($field);
        $value = $this->fixStepArgument($value);
        $this->getSession()->getPage()->fillField($field, $value);
    }



        /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }
}
