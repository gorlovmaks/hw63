<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phrase
 *
 * @ORM\Table(name="phrase")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PhraseRepository")
 */

use Knp\DoctrineBehaviors\Model as ORMBehaviors;

class Phrase
{

    use ORMBehaviors\Translatable\Translatable;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="orders")
     *
     *
     */
    private $client;


    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="User", inversedBy="phrase")
     *
     *
     */
    private $interpreter;

    /**
     * @return User
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param User $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return User
     */
    public function getInterpreter()
    {
        return $this->interpreter;
    }

    /**
     * @param User $interpreter
     */
    public function setInterpreter($interpreter)
    {
        $this->interpreter = $interpreter;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


}

