<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Phrase", mappedBy="client")
     */

    private $orders;

    /**
     * @var Phrase
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Phrase", mappedBy="interpreter")
     */

    private $phrase;

    public function __construct()
    {
        parent::__construct();
        $this->orders = new ArrayCollection();
    }

    /**
     * @return Phrase
     */
    public function getPhrase()
    {
        return $this->phrase;
    }

    /**
     * @param Phrase $phrase
     */
    public function setPhrase($phrase)
    {
        $this->phrase = $phrase;
    }
}
