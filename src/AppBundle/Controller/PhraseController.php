<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Phrase;
use AppBundle\Entity\PhraseTranslation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class PhraseController extends Controller
{
    /**
     * @Route("/")
     */
    public function index()
    {
        $phrases = $this->getDoctrine()
            ->getRepository('AppBundle:Phrase')
            ->findAll();


        return $this->render('@App/Phrase/index.html.twig', array(
            'phrases' => $phrases
        ));
    }

    /**
     * @Route("/newOrder")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newOrder(Request $request)
    {

        $client = $this->getUser();
        $form = $this->createFormBuilder()
            ->add('ruPhrase', TextType::class, [
                    'label' => $this
                        ->get('translator')
                        ->trans('Enter Russian Phrase'),

            ])
            ->add('save', SubmitType::class, [
                'label' => $this
                    ->get('translator')
                    ->trans('Save')
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            $phrase = new Phrase();

            /** @var  PhraseTranslation $ru_translation */

            $ru_translation = $phrase->translate('ru');

            $ru_translation->setPhrase($data['ruPhrase']);
            $phrase->setClient($client);
            $em = $this->getDoctrine()->getManager();
            $em->persist($phrase);

            $phrase->mergeNewTranslations();
            $em->flush();

            return $this->redirectToRoute('app_phrase_fulfillmentofanorder', array('id' => $phrase->getId()));
        }
        return $this->render('@App/Phrase/newOrder.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    /**
     * @Route("/phrase/{id}", requirements={"id": "\d+"})
     * @param Request $request
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function FulfillmentOfAnOrder(Request $request , int $id){

        $phrase = $this->getDoctrine()
            ->getRepository('AppBundle:Phrase')
            ->find($id);

        $form = $this->createFormBuilder()
            ->add('ipPhrase', TextType::class, [
                'label' => $this
                ->get('translator')
                ->trans('Enter Spanish translation'),
                'required' => false,
                'attr' => array('value' => $phrase->translate('ip')->getPhrase())
            ])
            ->add('enPhrase', TextType::class, [
                'label' => $this
                    ->get('translator')
                    ->trans('Enter English translation'),
                'required' => false,
                'attr' => array('value' => $phrase->translate('en')->getPhrase())
            ])
            ->add('frPhrase', TextType::class, [
                'label' => $this
                    ->get('translator')
                    ->trans('Enter the French translation'),
                'required' => false,
                'attr' => array('value' => $phrase->translate('fr')->getPhrase())
            ])
            ->add('dePhrase', TextType::class, [
                'label' => $this
                    ->get('translator')
                    ->trans('Enter German translation'),
                'required' => false,
                'attr' => array('value' => $phrase->translate('de')->getPhrase())
            ])
            ->add('itPhrase', TextType::class, [
                'label' => $this
                    ->get('translator')
                    ->trans('Enter the Italian translation'),
                'required' => false,
                'attr' => array('value' => $phrase->translate('it')->getPhrase())

            ])
            ->add('save', SubmitType::class, [
                'label' => $this
                    ->get('translator')
                    ->trans('Translate')
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            /** @var  PhraseTranslation $ip_translation */
            $ip_translation = $phrase->translate('ip',false);
            /** @var  PhraseTranslation $en_translation */
            $en_translation = $phrase->translate('en',false);
            /** @var  PhraseTranslation $fr_translation */
            $fr_translation = $phrase->translate('fr',false);
            /** @var  PhraseTranslation $de_translation */
            $de_translation = $phrase->translate('de',false);
            /** @var  PhraseTranslation $it_translation */
            $it_translation = $phrase->translate('it',false);

            $ip_translation->setPhrase($data['ipPhrase']);
            $en_translation->setPhrase($data['enPhrase']);
            $fr_translation->setPhrase($data['frPhrase']);
            $de_translation->setPhrase($data['dePhrase']);
            $it_translation->setPhrase($data['itPhrase']);


            $em = $this->getDoctrine()->getManager();
            $phrase->mergeNewTranslations();
            $em->persist($phrase);


            $em->flush();

            return $this->redirectToRoute('app_phrase_index');
        }


        return $this->render('@App/Phrase/phrasesTranslationList.html.twig', array(
            'form' => $form->createView(),
            'phrase' => $phrase
        ));
    }


    /**
     * @Route("/{_locale}/", requirements = {"_locale" : "en|ru"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
      public function locale(Request $request){

        return $this->redirect($request->headers->get('referer'));
      }

}
